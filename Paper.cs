using System;

namespace EX_28
{
    class Paper
    {
        private string paperCode;
        private string paperDescription;

        public string PaperCode
        {
            get
            {
                return paperCode;
            }

            set
            {
                paperCode = value;
            }
        }

        public String PaperDescription
        {
            get
            {
                return paperDescription;
            }
            set
            {
                paperDescription = value;
            }
        }


        public Paper()
        {

        }
    }
}