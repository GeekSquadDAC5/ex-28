using System;
using System.Collections.Generic;

namespace EX_28
{

    // The second class is student, with the following
    // Attributes:
    // - StudentId: Int
    // + Constructor : String, Int, Int
    // + Papers : List<String>
    // + ListAllPaper(): Void
    // + PrintFavouritePaper(string): Void

    public class Student : Person
    {
        private int StudentId;

        public List<string> Papers;

        public Student(string _name, int _age, int _studentId) : base(_name, _age)
        {
            StudentId = _studentId;
            Papers = new List<string>();
        }

// TEst
        // public void printOutInfo()
        // {
        //     Console.WriteLine(Name);
        //     Console.WriteLine(Age);
        //     Console.WriteLine(StudentId);
        // }


        public void ListAllPaper()
        {
            Console.Write("My papers are ");
            string paperList = string.Join(", ", Papers.ToArray());
            Console.WriteLine(paperList + ".");
        }

        public void PrintFavouritePaper(string _favouritePaper)
        {
            Console.WriteLine($"Favourite paper is {_favouritePaper}");
        }
    }

}