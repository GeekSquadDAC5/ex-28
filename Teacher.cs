using System;

namespace EX_28
{
    public class Teacher : Person
    {
        private string Paper;
        private int TeacherId;

        public Teacher(string _name, int _age, string _paper, int _teacherId) : base (_name, _age)
        {
            this.Paper = _paper;
            this.TeacherId = _teacherId;
        }

        public void ListPaper()
        {
            Console.WriteLine($"{Name}'s teaching paper is {this.Paper}");
        }
    }
}