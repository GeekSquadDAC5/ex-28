﻿using System;
using System.Collections.Generic;

namespace EX_28
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            // Create 2 students with papers they take at Dac5
            // Print out all 3 methods
            var student1 = new Student("Sing", 100, 10013041);

            student1.Papers.Add("IT Infrastructure");
            student1.Papers.Add("Software package");
            student1.Papers.Add("Intro to Programming");
            student1.Papers.Add("Professional Skills");
            student1.SayHello();
            student1.ListAllPaper();
            student1.PrintFavouritePaper(student1.Papers[0]);

            var student2 = new Student("Song", 200, 10015233);

            student2.Papers.Add("IT Infrastructure");
            student2.Papers.Add("Software package");
            student2.Papers.Add("Intro to Programming");
            student2.Papers.Add("Professional Skills");
            student2.SayHello();
            student2.ListAllPaper();
            student2.PrintFavouritePaper(student2.Papers[2]);

            // Create 4 teachers each with the paper they teach
            var teacher1 = new Teacher("Jeff", 300, "Intro to programing", 6574926);
            var teacher2 = new Teacher("Stefan", 300, "IT Infrastructure", 6574926);
            var teacher3 = new Teacher("Ray", 300, "Professional skills", 6574926);
            var teacher4 = new Teacher("Murray", 300, "Software packages", 6574926);

            // In the main method create a list with the Paper as the Type.
            // Create 4 new objects in this list using this sample code
            var papers = new List<Papers>();
            papers.Add(new Papers{PaperCode = "COMP5002", PaperDescription = "Intro to programing"});
            papers.Add(new Papers{PaperCode = "INFT5001", PaperDescription = "Professional skills"});
            papers.Add(new Papers{PaperCode = "COMP5008", PaperDescription = "Software packages"});
            papers.Add(new Papers{PaperCode = "COMP5004", PaperDescription = "IT Infrastructure"});

            // Create a foreach loop that prints out all the paper
            foreach(var paper in papers)
            {
                Console.WriteLine($"Paper Code : {paper.PaperCode}, Paper Description {paper.PaperDescription}");
            }
        }
    }
}
