using System;   

namespace EX_28
{

    // a) Create a program with 2 classes (in addition to the Program class)
    // The first class is person, with the following
    // Attributes:
    // - Name : String
    // - Age : Int
    // + Constructor : String, Int
    // + SayHello() : Void

    public class Person
    {
        protected String Name;
        private int Age;

        //public Person(){ }

        public Person(string _name, int _age)
        {
            Name = _name;
            Age = _age;
        }

        public void SayHello()
        {
            Console.WriteLine("Hello World");
            Console.WriteLine($"My name is {Name}, and I am {Age} years old");
        }
    }
}